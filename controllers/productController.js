const product = require('../models/product');
const user = require('../models/user');

// Create a new product
/*
module.exports.addProduct = (reqBody, userData) => {
	return user.findById(userData.userId).then(result => {
		// console.log(userData.isAdmin)
		if(userData.isAdmin == false) {
			return 'You are not an Admin!';
		} else {
			let newProduct = new product({
				name: reqBody.name,
				description: reqBody.description,
				price: reqBody.price
			});
			return newProduct.save().then((product, error) => {
				if(error) {
					return false;
				} else {
					return 'Product created successfully';
				};
			});
		};
	});
};
*/
// or
module.exports.addProduct = (reqParams, reqBody, data) => {
	if (data) {
		let newProduct = new product({
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		});
		return newProduct.save().then((product, error) => {
			if(error) {
				return false;
			} else {
				return 'Product created successfully';
			};
		});
	} else {
		return Promise.reject('You are not the admin');
	};
};

// Retrieve all products
module.exports.getAllProducts = () => {
	return product.find({}).then(result => {
		return result;
	});
};

// Retrieve single product
module.exports.getProduct = (reqParams) => {
	return product.findById(reqParams.productId).then(result => {
		return result;
	});
};

// Update product information
module.exports.updateProduct = (reqParams, reqBody, data) => {
	console.log(data)
	if (data) {
		let updatedProduct = {
			name: reqBody.name,
			description: reqBody.description,
			price: reqBody.price
		};
		return product.findByIdAndUpdate(reqParams.productId, updatedProduct).then((updatedProduct, error) => {
			if (error) {
				return false;
			} else {
				return 'Product information updated!'; //true;
			};
		});
	} else {
		return Promise.reject('You are not the admin!');
	};
};

// Archive a product
/*
module.exports.archiveProduct = (reqParams) => {
	return product.findByIdAndUpdate(reqParams.productId).then(result => {
		if (result.isActive == true) {
			let archivedProduct = {
				isActive: false
			}
			return archivedProduct;
		} else {
			return 'Product is already archived!';
		}
	})
}
*/

module.exports.archiveProduct = (reqParams, isUserAdmin) => {
	console.log(reqParams.productId);
	console.log(isUserAdmin);
	if (isUserAdmin) {
		let archivedProduct = {
			 isActive: false
		};
		return product.findByIdAndUpdate(reqParams.productId, archivedProduct).then((archivedProduct, error) => {
			if (error) {
				return false;
			} else {
				// console.log(archivedProduct.isActive);

				// archivedProduct.isActive = false;
				// console.log(archivedProduct.isActive);
				return 'Successfully archived the product!'; //true;
			};
		});
	} else {
		return Promise.reject('You are not the admin!');
	};
};



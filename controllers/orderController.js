const product = require('../models/product');
const order = require('../models/order')
const user = require('../models/user');

module.exports.createOrder =  (reqBody, userData) => {
    return user.findById(userData.userId).then((result) => {
        // console.log(userData.isAdmin)
        if(userData.isAdmin) {
            return 'Not valid to create order.';
        } else {
            let newOrder = new order({
                userId: userData.userId,  
                productId: reqBody.productId,
                productName: reqBody.productName,
                quantity: reqBody.quantity,
                totalAmount: reqBody.totalAmount  
            });
            return newOrder.save().then((order, error)=> {   
                if(error){
                    return false
                } else {
                    return 'Order created!'
                }
            })
        }
    })
};


const user = require('../models/user');
const product = require('../models/product');
const bcrypt = require('bcrypt');
const auth = require('../auth');

// User registration
module.exports.registerUser = (reqBody) => {
	let newUser = new user ({
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	});
	return newUser.save().then((user, error) => {
		if (error) {
			return false;
		} else {
			return true;
		}
	});
};

// User authentication
module.exports.loginUser = (reqBody) => {
	return user.findOne({email: reqBody.email}).then(result => {
		if (result == null) {
			return false;
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);
			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result)};
			} else {
				return false;
			};
		};
	});
};

// User creates order
/*
module.exports.userCreateOrder = (data, userData) => {
	console.log(data);
	console.log(userData)
	if (userData.isAdmin == false) {
		user.orders.products.push({
			productName: data.productName,
			quantity: data.quantity
		}),
		user.orders.push({
			totalAmount: data.totalAmount
		})
		return user.save().then((user, error) => {
			if (error) {
				return false;
			} else {
				return true;
			};
		})
	} else {
		return false;
	}
};
*/

module.exports.getUserDetails = (userData) => {
	return user.findById(userData.id).then(result => {
		console.log(userData)
        if(result == null) {
            return false;
        } else {
            console.log(result)
            result.password="******";
            return result
        }
    })
};

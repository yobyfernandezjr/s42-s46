const express = require('express');
const router = express.Router();
const productController = require('../controllers/productController');
const auth = require('../auth');

// Route for creating product by admin
/*
router.post('/', auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization);
	productController.addProduct(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => {
		res.send(resultFromController);
	});
});
*/
//or
router.post('/', auth.verify, (req, res) => {
	const isAdminUser = auth.decode(req.headers.authorization).isAdmin;
	productController.addProduct(req.params, req.body, isAdminUser)
		.then(resultFromController => res.send(resultFromController))
		.catch(error => res.send(error))
})


// Route for retrieving products
router.get('/', (req, res) => {
	productController.getAllProducts().then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route to retrieve single product
router.get('/:productId', (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params).then(resultFromController => {
		res.send(resultFromController);
	});
});

// Route to update a product by admin 
router.put('/:productId', auth.verify, (req, res) => {
	const isAdminUser = auth.decode(req.headers.authorization).isAdmin;
	console.log(isAdminUser);
	productController.updateProduct(req.params, req.body, isAdminUser)
		.then(resultFromController => res.send(resultFromController))
		.catch(error => res.send(error))
	// });
});

// Route for archiving product by admin
router.put('/:productId/archive', auth.verify, (req, res) => {
	const isAdminUser = auth.decode(req.headers.authorization).isAdmin;
	console.log(isAdminUser);
	productController.archiveProduct(req.params,  isAdminUser)
		.then(resultFromController => res.send(resultFromController))
		.catch(error => res.send(error))
	//});
});

module.exports = router;
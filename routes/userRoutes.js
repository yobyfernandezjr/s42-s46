const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const auth = require('../auth');

// Route for user registration
router.post('/register', (req, res) => {
    userController.registerUser(req.body).then(resultFromController => {
    		res.send(resultFromController)
    });
});

// Route for user authentication
router.post('/login', (req, res) => {
	userController.loginUser(req.body).then(resultFromController => {
		res.send(resultFromController)
	});
});

// Route to create order
/*
router.post('/orders', auth.verify,  (req, res) => {
	const userData = auth.decode(req.headers.authorization);

	userController.userCreateOrder(req.body, {
		userId: userData.id,  
		isAdmin: userData.isAdmin
	})
		.then(resultFromController => res.send(resultFromController))
		.catch(error => res.send(error))
});
*/
//s45 Retrieve User Details
router.get("/details", auth.verify, (req, res)=>{
    
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    userController.getUserDetails({id: userData.id})
    .then(resultFromController => 
        res.send(resultFromController))
});


module.exports = router;

const express = require('express');
const router = express.Router();
const auth = require('../auth');
const orderController = require('../controllers/orderController');

router.post('/', auth.verify, (req,res)=>{
    const userData = auth.decode(req.headers.authorization)
    console.log(userData)

    orderController.createOrder(req.body, {userId: userData.id, isAdmin: userData.isAdmin}).then(resultFromController => 
        res.send(resultFromController))
});

module.exports = router

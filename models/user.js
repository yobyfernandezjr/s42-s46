const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	email: {
		type: String,
		required: [true, 'Email address is required!']
	},
	// userName: {
	// 	type: String,
	// 	required: [true, 'User name is required!']
	// },
	password: {
		type: String,
		required: [true, 'Password is required!']
	},
	isAdmin: {
		type: Boolean,
		default: false
	}
/*
	orders: [
		{
			products: [
				{
					productName: {
						type: String,
						required: [true, 'Product name is required!']
					},				
					quantity: {
						type: Number,
						required: [true, 'Quantity is required!']
					}
				}
			],
			totalAmount: {
				type: Number,
				required: [true, 'Amount is required!']
			},
			purchasedOn: {
				type: Date,
				default: new Date()
			}
		}
	]
*/
});

module.exports = mongoose.model('user', userSchema);